Answer :

1.  Hitung jumlah vote untuk Sen. Olympia Snowe yang memiliki id 524.

        answer :

2.  Sekarang lakukan JOIN tanpa menggunakan id 524. Query kedua tabel votes dan congress_members.

        answer :

3.  Sekarang gimana dengan representative Erik Paulsen? Berapa banyak vote yang dia dapatkan ?

        answer :

4.  Buatlah daftar peserta Congress yang mendapatkan vote terbanyak. Jangan sertakan field created_at dan updated_at.

        answer :

5.  Sekarang buatlah sebuah daftar semua anggota Congress yang setidaknya mendapatkan beberapa vote dalam urutan dari yang paling sedikit. Dan juga jangan sertakan field-field yang memiliki tipe date.

        answer :

6.  Siapa anggota Congress yang mendapatkan vote terbanyak? List nama mereka dan jumlah vote-nya. Siapa saja yang memilih politisi tersebut? List nama mereka, dan jenis kelamin mereka.

        answer :

7.  Berapa banyak vote yang diterima anggota Congress yang memiliki grade di bawah 9 (gunakan field grade_current)? Ambil nama, lokasi, grade_current dan jumlah vote.

        answer :

8.  Apa saja 10 negara bagian yang memiliki voters terbanyak? List semua orang yang melakukan vote di negara bagian yang paling populer. (Akan menjadi daftar yang panjang, kamu bisa gunakan hasil dari query pertama untuk menyederhanakan query berikut ini.)

        answer :

9.  List orang-orang yang vote lebih dari dua kali. Harusnya mereka hanya bisa vote untuk posisi Senator dan satu lagi untuk wakil. Wow, kita dapat si tukang curang! Segera laporkan ke KPK!!

        answer :

10. Apakah ada orang yang melakukan vote kepada politisi yang sama dua kali? Siapa namanya dan siapa nama politisinya ?

        answer :
