let arr_1 = [3, 5, 22, 5,  7,  2,  45, 75, 89, 21, 2]; //output --> 276
let arr_2 = [9, 2, 42, 55, 71, 22, 4,  5,  90, 25, 26];  //output --> 351

function sumTwoArray() {
  // write code here
}

// driver code
console.log(sumTwoArray(arr_1, arr_2)) // 627

// Example output: // 276 + 351 = 627
